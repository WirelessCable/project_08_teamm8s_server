package com.example.demo.service;

import com.example.demo.model.Menu;
import com.example.demo.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;



    public List<Menu> getMenu(){

          return postRepository.findAll();

    }

}
