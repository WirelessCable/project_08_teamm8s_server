package com.example.demo.controller;

import com.example.demo.model.Menu;
import com.example.demo.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;




    @GetMapping("/menu")
    public List<Menu> getMenu(){

        return postService.getMenu();

    }
}
